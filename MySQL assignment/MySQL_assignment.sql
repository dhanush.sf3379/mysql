-- Assignment 1

create table providers (
		code varchar(255) not null,
        name varchar(255) not null
);

insert into providers(code,name)
	values('HAL','Clarke Enterprises'),
			('RBT','Susan Calvin Corp'),
            ('TNBC','Skellington Supplies');

create table pieces (
		code int not null,
        name varchar(255) not null,
        primary key(code)
);

insert into pieces(code,name)
	values(1,'Sprocket'),
			(2,'Screw'),
            (3,'Nut'),
            (4,'Bolt');
            

create table provides(
		piece int not null,
        provider varchar(255) not null,
        price int not null

);     

insert into provides(piece,provider,price)
	values(1,'HAL',10) ,
			(1,'RBT',15),
            (2,'HAL',20),
            (2,'RBT',15),
            (2,'TNBC',14),
            (3,'RBT',50),
            (3,'TNBC',45),
            (4,'HAL',5),
            (4,'RBT',7);

-- Assignment 1 Excercise - 1

select name from pieces;

-- Assignment 1 Excercise - 2

select * from providers;

-- Assignment 1 Excercise - 3



		
-- Assignment 1 Excercise - 4

select provider from provides
	where piece=1;
    
-- Assignment 1 Excercise - 5

select Name from pieces p
join provides ps on p.code=ps.piece
where provider='HAL'


-- Assignment 1 exercise - 7

insert into provides 
	values(1,'TNBC',7);
    

-- Assignment 1 exercise - 8

update provides 
set price=price+1;
select * from provides;

-- Assignment 1 exercise - 9


-- Assignment 2 exercise - 1
-- select * from order_details;
select orderID ,UnitPrice,Quantity,UnitPrice*Quantity AS Subtotal from order_details;


-- Assignment 2 exercise - 2
-- select * from products
select ProductId,ProductName from products
where Discontinued='y'
order by ProductName;


-- Assignment 2 exercise - 3
select ProductName as Ten_Most_Expensive_Products, UnitPrice from products 
order by UnitPrice desc
limit 10;

-- Assignment 2 exercise - 4

select c.CategoryName,p.ProductName,p.QuantityPerUnit,p.UnitsInStock,p.Discontinued from Categories c
join Products p using(CategoryID)
where p.Discontinued ='n'
order by c.CategoryName;

-- Assignment 2 exercise - 5

-- select * from Suppliers-- 
-- select * from Customers
select c.City,c.CompanyName,c.ContactName from Customers c
left join Suppliers s on c.City=s.City
order by city
    








